const { React } = window;

const LogIn = () => {
    return (
        <div className="LogInForm" id="login">
            <div>
                <label className="label"> User </label>
                <input className="input" id="user"></input>
            </div>
            <div>
                <label className="label"> Password </label>
                <input className="input" id="password"></input>
            </div>
            <button id="log">Log in</button>
        </div>
    )
};