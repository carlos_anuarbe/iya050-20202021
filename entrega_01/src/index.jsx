const { React, ReactDOM } = window;
const { data } = window;
const { BrowserRouter: Router, NavLink, Switch, Route } = window.ReactRouterDOM;
const { useState } = React;

//home page component to start
const App = function () {
    return (
        <div>
            <Router>
                <NavBar />
                <Switch>
                    <Route exact path="/index.html">
                    </Route>
                    <Route path="/Login">
                        <LogIn />
                    </Route>
                    <Route path="/index.html/Main">
                        <Main data={data} />
                    </Route>
                    <Route path="/index.html/Signin">
                        <SignIn />
                    </Route>
                </Switch>
            </Router>
        </div>
    );
}

//Navegation bar for show links
const NavBar = () => {
    return (
        <div className="header" id="nav">
            <NavLink id="a" to="/index.html">Home</NavLink>
            <NavLink to="/Login">Login</NavLink>
            <NavLink to="/Main">Main</NavLink>
            <NavLink to="/Signin">Signin</NavLink>
        </div>
    )
}

ReactDOM.render(<App />, document.getElementById("root"));
